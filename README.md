# Installation

Download Godot Engine here : https://godotengine.org/download/windows

```bash
git clone https://gitlab.com/RanuSenpai/godotfirstgame.git 
```
Open it with Godot Engine !

# Tutorial

This project is made with GDQuest Tutoriel here : 
- Part 1 : https://www.youtube.com/watch?v=Mc13Z2gboEk&ab_channel=GDQuest (Player and Enemy)
- Part 2 : https://www.youtube.com/watch?v=6ziIyx60N6I&ab_channel=GDQuest (Coins, Portals, and Levels)
- Part 3 : https://www.youtube.com/watch?v=mjWwWIEyib8&ab_channel=GDQuest (Menus, Pause, and Score)


# Live coding

To learn more deeply, I improve and add objects, features, levels etc... on live twitch  : https://www.twitch.tv/ranusenpai

Todo List [Board](https://gitlab.com/RanuSenpai/godotfirstgame/-/boards) : 
- Create a good scenario of several scenes by increasing the difficulty (Game design) [Issue](https://gitlab.com/RanuSenpai/godotfirstgame/-/issues/1)
- Create a trampoline to access high places [Issue](https://gitlab.com/RanuSenpai/godotfirstgame/-/issues/2)


